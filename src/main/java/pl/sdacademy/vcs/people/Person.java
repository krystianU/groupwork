package pl.sdacademy.vcs.people;

import java.util.Objects;

public abstract class Person {
    private String name;
    private String surname;
    private long pesel;

    private Gender gender;

    public Person(String name, String surname, long pesel, Gender gender) {
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
        this.gender = gender;

        if (!peselCheckSum(this.pesel)) {
            throw new IllegalArgumentException("Pesel invalid");
        } else {
            System.out.println("Pesel is valid");
        }
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public long getPesel() {
        return pesel;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPesel(long pesel) {
        this.pesel = pesel;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Person)) {
            return false;
        }
        Person person = (Person) o;
        return Objects.equals(name, person.name) &&
                Objects.equals(surname, person.surname) &&
                Objects.equals(pesel, person.pesel) &&
                Objects.equals(gender, person.gender);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, pesel, gender);
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", pesel='" + pesel + '\'' +
                ", gender=" + gender +
                '}';
    }

    public boolean peselCheckSum(long pesel) {
        long[] digits = new long[11];
        for (int i = 10; i >= 0; i--) {
            digits[i] = pesel % 10;
            pesel = pesel / 10;
        }
        long[] weights = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3};
        int check = 0;
        for (int i = 0; i < 10; i++) {
            check += weights[i] * digits[i];
        }
        int lastNumber = check % 10;
        int controlNumber = 10 - lastNumber;

        if (controlNumber == digits[10]) {
            return true;
        }
        return false;
    }

}
