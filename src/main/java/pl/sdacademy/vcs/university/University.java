package pl.sdacademy.vcs.university;

import pl.sdacademy.vcs.people.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/*
This class should be immutable
*/

public class University {
    private String name;
    private List<Register> registers = new ArrayList<>();
    private Register reg1 = new Register(name + "reg1");

    University(String name) {
        this.name = name;
        reg1.setName(name + " * reg1");
    }

    University(String name, List<Register> registers) {
        this.name = name;
        this.registers = registers;
        reg1.setName(this.name + " % reg1");
    }

    public void add(Register areg) {
        for (Person a : areg.getStudents()) {

            if (a == null) {
                System.out.println("Osoba nie istnieje");
            } else {

                reg1.add(a);
            }
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Register> getRegisters() {
        return registers;
    }

    public void setRegisters(List<Register> registers) {
        this.registers = registers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        University that = (University) o;
        return Objects.equals(name, that.name) ||
                Objects.equals(registers, that.registers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, registers);
    }

    public void remove() {
        this.name = null;
        this.registers = null;
    }

    public Register getReg1() {
        return reg1;
    }

    public void setReg1(Register reg1) {
        this.reg1 = reg1;
    }
}
