package pl.sdacademy.vcs.university;

import java.util.LinkedList;
import java.util.List;

public abstract class UniverityManager {
    private static  List<University> UniWers = new LinkedList<University>();
    public static University addUni(String UniverityName){
        University UniWers1 = new University(UniverityName);
        UniWers.add(UniWers1);
        return UniWers1;
    }
    public static void removeUni(String UniversityName) {
        for (University a : UniWers) {
            if (a.getName().equals(UniversityName)) {
                a.remove();
            }
        }
    }

    public static List<University> getUniWers() {
        return UniWers;
    }
}
