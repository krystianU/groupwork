package pl.sdacademy.vcs.people;

import pl.sdacademy.vcs.people.Gender;
import pl.sdacademy.vcs.people.Person;

public class Student extends Person {
    public Student(String name, String surname, long pesel, Gender gender) {
        super(name, surname, pesel, gender);
    }
}
