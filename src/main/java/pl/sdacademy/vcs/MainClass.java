package pl.sdacademy.vcs;

import pl.sdacademy.vcs.people.Gender;
import pl.sdacademy.vcs.people.Person;
import pl.sdacademy.vcs.people.Student;
import pl.sdacademy.vcs.university.Register;
import pl.sdacademy.vcs.university.UniversityManager;
import pl.sdacademy.vcs.university.University;

import java.util.Arrays;

public class MainClass {

    /*
    This method is doing to many various things.
    It's not it's responsibility (due to SOLID).
     */
    public static void main(String[] args) {
        Register usos = new Register("USOS");
        Student p1 = new Student("Tomek", "Bartosz", 95102603774L, Gender.MALE);
        Student p2 = new Student("Bartek", "Tomasz", 95102603774L, Gender.MALE);
        usos.addStudent(p1);
        usos.addStudent(p2);


        University uj = UniversityManager.addUni("uj");
        uj.add(usos);

        University agh = UniversityManager.addUni("agh");
        agh.add(usos);

        for (University u : Arrays.asList(uj, agh)) {
            // Should it then have private (package-private) constructor?
            System.out.println("University: " + u.getName());

            System.out.println("Register: " + u.getReg1().getName());
            for (Person person : u.getReg1().getStudents()) {
                System.out.println("Student: (" + (person.getGender()) + ") " + person.getName() + " " + person.getSurname() + ", " + person.getPesel());
            }
            System.out.println();

            System.out.println();
        }
    }
}
