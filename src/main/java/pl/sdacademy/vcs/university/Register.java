package pl.sdacademy.vcs.university;

import pl.sdacademy.vcs.people.Gender;
import pl.sdacademy.vcs.people.Person;
import pl.sdacademy.vcs.people.Student;

import java.util.ArrayList;
import java.util.List;

public class Register {

    private String name;
    private List<Person> students = new ArrayList<>();

    private Person keeper = new Student("Default", "Default", 93052407093L, Gender.MALE);

    public void setKeeper(Person keeper) {
        if (students.contains(keeper)) {
            System.out.println("Student nie może być studentem");
        } else {
            this.keeper = keeper;
        }
    }

    public Person getKeeper() {
        return keeper;
    }

    public Register(String name) {
        this.name = name;
    }

    public void addStudent(Person student) {
        students.add(student);
    }

    public List<Person> getStudents() {
        return students;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStudents(List<Person> students) {
        this.students = students;
    }

    public void add(Person a) {
        if (a != null) {
            this.students.add(a);
        }
    }

}
