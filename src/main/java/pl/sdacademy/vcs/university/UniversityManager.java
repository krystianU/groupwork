package pl.sdacademy.vcs.university;

import java.util.LinkedList;
import java.util.List;

public abstract class UniversityManager {
    private static List<University> UniVers = new LinkedList<University>();

    public static University addUni(String UniversityName) {
        University UniVers1 = new University(UniversityName);
        UniVers.add(UniVers1);
        return UniVers1;
    }

    public static void removeUni(String UniversityName) {

        UniVers.stream()
                .filter(s -> s.getName().equals(UniversityName))
                .forEach(University::remove);

    }

    public static List<University> getUniWers() {
        return UniVers;
    }
}
